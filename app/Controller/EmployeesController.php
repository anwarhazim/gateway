<?php
class EmployeesController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'forgot', 'setup');
    }

    public function index()
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Gender');
        $this->loadModel('Ethnic');
        $this->loadModel('Marital');
        $this->loadModel('State');
        $this->loadModel('JobDesignation');
        $this->loadModel('JobGrade');
        $this->loadModel('Organisation');
        $this->loadModel('Position');
        $this->loadModel('PersonalArea');
        $this->loadModel('PersonalSubarea');
        $this->loadModel('EmployeeGroup');
        $this->loadModel('EmployeeSubgroup');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($employee))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Notifications', 'action' => 'index'));
        }

        if(!empty($employee['Personal']['date_of_birth']))
        {
            $employee['Personal']['date_of_birth'] =  date("d-m-Y", strtotime($employee['Personal']['date_of_birth']));
        }

        if(!empty($employee['Employee']['seniority_date']))
        {
            $employee['Employee']['seniority_date'] =  date("d-m-Y", strtotime($employee['Employee']['seniority_date']));
        }

        if(!empty($employee['Employee']['entry_date']))
        {
            $employee['Employee']['entry_date'] =  date("d-m-Y", strtotime($employee['Employee']['entry_date']));
        }

        if(!empty($employee['Employee']['leaving_date']))
        {
            $employee['Employee']['leaving_date'] =  date("d-m-Y", strtotime($employee['Employee']['leaving_date']));
        }

        $jobdesignations = $this->JobDesignation->find('list');
        $jobgrades = $this->JobGrade->find('list');
        $positions = $this->Position->find('list');
        $personalareas = $this->PersonalArea->find('list');
        $personalsubareas = $this->PersonalSubarea->find('list');
        $employeegroups = $this->EmployeeGroup->find('list');
        $employeesubgroups = $this->EmployeeSubgroup->find('list');
        $genders = $this->Gender->find('list');
        $ethnics = $this->Ethnic->find('list');
        $marital = $this->Marital->find('list');
        $states = $this->State->find('list');

        $organisationunit = $this->Organisation->findById($employee['Employee']['organizational_unit_id']);

        $organisations = $this->Organisation->find('list',
                                                array(
                                                    'conditions' => array(
                                                                        'Organisation.batch_organisation_id' => $organisationunit['Organisation']['batch_organisation_id'],
                                                                        'Organisation.is_active' => 1,
                                                                        'Organisation.is_deleted' => 99,
                                                                    ),
                                                ));

        $this->request->data = $employee;

        $path = Router::url('/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/', true);

        $disabled = 'disabled';

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // login
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

		$this->set(compact('employee', 'path', 'disabled', 'states', 'genders', 'ethnics', 'marital', 'jobdesignations', 'jobgrades', 'positions', 'organisations', 'personalareas', 'personalsubareas', 'employeegroups', 'employeesubgroups'));
	}

	public function colleague()
	{
        $this->loadModel('Employee');
        $this->loadModel('Gallery');
        $this->loadModel('Project');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $project = $this->Project->findById(1);

        $conditions = array();

        $conditions['conditions'][] = array();


        $conditions['conditions'][] = array(
                                            'Employee.is_active' => 1,
                                            'Employee.division_code' => $employee['Employee']['division_code'],
                                        );
             

        $conditions['order'] = array('Employee.modified'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Employee'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Personal.complete_name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Employee.employee_no LIKE' => '%' . $value . '%')
                        );
                    } 

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Employee'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Employee');

        for ($i=0; $i < count($details); $i++) 
        { 
            if(!empty($details[$i]['Employee']['entry_date']))
            {
                $details[$i]['Employee']['entry_date'] = date("d-m-Y",strtotime($details[$i]['Employee']['entry_date']));
            }
            else
            {
                $details[$i]['Employee']['entry_date'] = '-';
            }

            if(!empty($details[$i]['Employee']['leaving_date']))
            {
                $details[$i]['Employee']['leaving_date'] = date("d-m-Y",strtotime($details[$i]['Employee']['leaving_date']));
            }
            else
            {
                $details[$i]['Employee']['leaving_date'] = '-';
            }

            $gallery = $this->Gallery->find('first', array(
											'conditions' => array('Gallery.user_id' => $details[$i]['Personal']['user_id'], 'Gallery.is_active' => 1)
										));
										
            $details[$i]['Gallery']['name'] =  Router::url('/app/webroot/images/placeholders/placeholder.jpg', true);;
            if(!empty($gallery))
            {
                $path = Router::url($project['Project']['url'].'/app/webroot/documents/'.$details[$i]['Employee']['employee_no'].'/MEDIAS/', true);
                $details[$i]['Gallery']['name'] = $path.$gallery['Gallery']['name'];
            }
            
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('details'));
    }
}