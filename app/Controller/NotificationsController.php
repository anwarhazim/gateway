<?php
class NotificationsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
	}
	
	public function index()
	{
        $this->loadModel('Log');
        $this->loadModel('Project');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $project = $this->Project->findById(1);
        
        $path = $project['Project']['url'].'/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/';

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Notification.recipient_id' => $employee['EmployeeList'],
                                        );

        $conditions['order'] = array('Notification.id'=> 'DESC');


        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Notification'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Notification.subject LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Notification.body LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Notification.created) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Notification.created) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Notification'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        { 
            $sender = $this->Utility->getEmployeeSummary($details[$i]['Notification']['sender_id']);
            $recipient = $this->Utility->getEmployeeSummary($details[$i]['Notification']['recipient_id']);

            $details[$i]['Notification']['id'] = $this->Utility->encrypt($details[$i]['Notification']['id'], 'ntf');

            $details[$i]['Notification']['modified'] = $this->Utility->datetime($details[$i]['Notification']['modified']);

            $details[$i]['Notification']['created'] = $this->Utility->datetime($details[$i]['Notification']['created']);

            if(!empty($sender))
            {
                $details[$i]['Sender'] = $sender['Employee'];
                $details[$i]['Sender']['complete_name'] = $this->Utility->strlen($sender['Employee']['complete_name'], 20);
            }

            if(!empty($recipient))
            {
                $details[$i]['Recipient'] = $recipient['Employee'];
                $details[$i]['Recipient']['complete_name'] = $this->Utility->strlen($recipient['Employee']['complete_name'], 20);
            }

            $details[$i]['Notification']['body'] = $this->Utility->strlen($details[$i]['Notification']['body'], 100);
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('details', 'path', 'employee'));
    }
    
    public function view($key = null)
    {
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $project = $this->Project->findById(1);

        $path = $project['Project']['url'].'/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/';

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'ntf');

        $detail = $this->Notification->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            if($detail['Notification']['is_read'] == 99)
            {
                $update = array();

                $update['Notification']['id'] = $detail['Notification']['id'];
                $update['Notification']['is_read'] = 1;
                $update['Notification']['modified_by'] = $employee['Employee']['id'];

                $this->Notification->create();
                $this->Notification->save($update);
            }
        }

        if(!empty($detail))
        {
            $sender = $this->Utility->getEmployeeSummary($detail['Notification']['sender_id']);
            
            $recipient = $this->Utility->getEmployeeSummary($detail['Notification']['recipient_id']);
            
            $detail['Notification']['modified'] = $this->Utility->datetime($detail['Notification']['modified']);

            $detail['Notification']['created'] = $this->Utility->datetime($detail['Notification']['created']);

            if(!empty($sender))
            {
                $detail['Sender'] = $sender['Employee'];
            }

            if(!empty($recipient))
            {
                $detail['Recipient'] = $recipient['Employee'];
            }
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('detail', 'path', 'employee'));
    }

    public function getNotificationCountByUserId()
    {
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $count = $this->Notification->find('count', array(
                                                        'conditions' => array('Notification.recipient_id' => $employee['EmployeeList'], 'Notification.is_read' => 99),
                                                    ));
        
        return $count;
    }

    public function getNotificationByUserId()
    {
        $this->loadModel('Project');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $project = $this->Project->findById(1);

        $notifications = $this->Notification->find('all', array(
                                                        'conditions' => array('Notification.recipient_id' => $employee['EmployeeList'], 'Notification.is_read' => 99),
                                                    ));

        $details = array();

        if(!empty($notifications))
        {
            foreach ($notifications as $notification) 
            {
                $sender = $this->Utility->getEmployeeSummary($notification['Notification']['sender_id']);

                $path = Router::url('/app/webroot/images/placeholders/placeholder.jpg', true);
                if(!empty($sender['Employee']['avatar']))
                {
                    $path = $project['Project']['url'].'/app/webroot/documents/'.$sender['Employee']['employee_no'].'/MEDIAS/'.$sender['Employee']['avatar'];
                }
    
                $img = "<img class='img-circle img-sm' src='".$path."'/>";

                $baseURL = Router::url('/', true);

                $details[$notification['Notification']['id']] = '<li class="media"><div class="media-left">'. $img .'</div>
                                                                    <div class="media-body">
                                                                            <a href="'.$baseURL.'Notifications/read/'.$this->Utility->encrypt($notification['Notification']['id'], 'ntf').'" class="media-heading">
                                                                                <span class="text-semibold">'.$sender['Employee']['complete_name'].'</span>
                                                                                <span class="media-annotation pull-right">'.$this->Utility->datetime($notification['Notification']['created']).'</span>
                                                                            </a>
                                                                            <span class="text-muted">'.$this->Utility->strlen($notification['Notification']['subject'], 40).'</span>
                                                                        </div>
                                                                    </li>';
            }
        }
        else
        {
            $details[] = '<li class="media">
                                <div class="media-body">
                                    <span class="text-muted">No notification...</span>
                                </div>
                            </li>';
        }

        $myJSON = json_encode($details);

        return $myJSON;
    }
}