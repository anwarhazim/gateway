<?php
class UsersController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'forgot', 'setup');
    }
	
	public function login()
	{
        $this->loadModel('Log');
        $this->loadModel('Utility');
		
		if($this->request->is('post') || $this->request->is('put'))
        {
			if ($this->Auth->login()) 
            { 
                $person = $this->Auth->user();
                
				$employee = $this->Utility->getUserInformation($person['id']);

                $session = "";

                $data = array();
                $data['User']['id'] = $person['id'];
                $data['User']['session'] = $session;

                $this->User->create();
                $this->User->save($data);

                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '1'; // login
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '4'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Welcome, '.$employee['Personal']['complete_name'], 'success');
                $this->redirect(array('controller' => 'Notifications', 'action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('You have entered an invalid Staff No. or Password. Please try again!', 'error');
			}
		}

		$this->layout = 'blank';
	}

	public function logout()
    {
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '8'; // log
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

        $this->redirect($this->Auth->logout());
    }

	public function profile()
	{
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Gender');
        $this->loadModel('Ethnic');
        $this->loadModel('Marital');
        $this->loadModel('State');
        $this->loadModel('JobDesignation');
        $this->loadModel('JobGrade');
        $this->loadModel('Organisation');
        $this->loadModel('Position');
        $this->loadModel('PersonalArea');
        $this->loadModel('PersonalSubarea');
        $this->loadModel('EmployeeGroup');
        $this->loadModel('EmployeeSubgroup');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($employee))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Notifications', 'action' => 'index'));
        }

        if(!empty($employee['Personal']['date_of_birth']))
        {
            $employee['Personal']['date_of_birth'] =  date("d-m-Y", strtotime($employee['Personal']['date_of_birth']));
        }

        if(!empty($employee['Employee']['seniority_date']))
        {
            $employee['Employee']['seniority_date'] =  date("d-m-Y", strtotime($employee['Employee']['seniority_date']));
        }

        if(!empty($employee['Employee']['entry_date']))
        {
            $employee['Employee']['entry_date'] =  date("d-m-Y", strtotime($employee['Employee']['entry_date']));
        }

        if(!empty($employee['Employee']['leaving_date']))
        {
            $employee['Employee']['leaving_date'] =  date("d-m-Y", strtotime($employee['Employee']['leaving_date']));
        }

        $roles = array();

        foreach ($employee['Roles'] as $role) 
        {
            $roles[$role['id']] = $role['name'];
        }

        $jobdesignations = $this->JobDesignation->find('list');
        $jobgrades = $this->JobGrade->find('list');
        $positions = $this->Position->find('list');
        $personalareas = $this->PersonalArea->find('list');
        $personalsubareas = $this->PersonalSubarea->find('list');
        $employeegroups = $this->EmployeeGroup->find('list');
        $employeesubgroups = $this->EmployeeSubgroup->find('list');
        $genders = $this->Gender->find('list');
        $ethnics = $this->Ethnic->find('list');
        $marital = $this->Marital->find('list');
        $states = $this->State->find('list');

        $organisationunit = $this->Organisation->findById($employee['Employee']['organizational_unit_id']);

        $organisations = $this->Organisation->find('list',
                                                array(
                                                    'conditions' => array(
                                                                        'Organisation.batch_organisation_id' => $organisationunit['Organisation']['batch_organisation_id'],
                                                                        'Organisation.is_active' => 1,
                                                                        'Organisation.is_deleted' => 99,
                                                                    ),
                                                ));

        $this->request->data = $employee;

        $path = Router::url('/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/', true);

        $disabled = 'disabled';

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

		$this->set(compact('employee', 'path', 'disabled', 'states', 'genders', 'ethnics', 'marital', 'jobdesignations', 'jobgrades', 'positions', 'organisations', 'personalareas', 'personalsubareas', 'employeegroups', 'employeesubgroups', 'roles'));
	}

	public function password()
	{
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$employee = $this->Utility->getUserInformation($person['id']);
		
		$detail = $this->User->find('first', array(
												'conditions' => array(
																	'User.is_active' => 1, 
																	'User.id'=> $person['id']
																)
											));

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Notifications', 'action' => 'index'));
		}
		
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['User']['id'] = $detail['User']['id'];
            
            $this->User->set($data);
            if($this->User->validates())
            {
                $this->User->create();
                $this->User->save($data);

                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '4'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');
                
                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Your new password successfully updated.', 'success');
                $this->redirect(array('action' => '/password'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $path = Router::url('/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/', true);

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '4'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

		$this->set(compact('employee', 'path'));
    }
}