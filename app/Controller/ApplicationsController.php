<?php
class ApplicationsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
	}
	
    public function index()
    {
        $this->loadModel('Employee');
        $this->loadModel('Setting');
        $this->loadModel('Modul');
        $this->loadModel('User');
        $this->loadModel('Project');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $applications = "";
        $modul_selected = array();
		$project_selected = array();
		$settings = array();
		$moduls = array();

        $user = $this->User->findById($employee['User']['id']);

		foreach ($user['UserRole'] as $role) 
		{

			$settings = $this->Setting->find('all',
											array(
												'conditions' => array('role_id' => $role['role_id'])	
											));

			if($settings)
			{
				foreach ($settings as $setting) 
				{
					if (!in_array($setting['Setting']['modul_id'], $modul_selected)) 
					{
						array_push($modul_selected ,$setting['Setting']['modul_id']);
					}
				}
			}
		}

		
    	
    	$moduls = $this->Modul->find('all',
									array(
										'conditions' => array(
															'Modul.id' => $modul_selected,
															'Modul.is_active' => 1,
															'Modul.is_nav' => 1,
														),
										'group' => 'Modul.project_id',
										'order' => array('Modul.order asc')	
									));

		foreach ($moduls as $modul) 
		{
			$project_selected[] = $modul['Modul']['project_id'];
        }

        foreach ($project_selected as $project_id) 
		{
			$project = $this->Project->findById($project_id);
			if(!empty($project))
			{
                $applications .= '<div class="col-md-3">';
                $applications   .= '<div class="panel">';
                $applications       .= '<div class="panel-body text-center">';
                $applications           .= '<div class="icon-object border-'.$project['Project']['color'].' text-'.$project['Project']['color'].'"><i class="icon-reading"></i></div>';
                $applications           .= '<h5 class="text-semibold">'.$project['Project']['name'].'</h5>';
                $applications           .= '<p class="mb-15">'.$project['Project']['desc'].'</p>';
                $applications           .= '<a href="'.$project['Project']['url'].'" class="btn bg-'.$project['Project']['color'].' legitRipple">Browse</a>';
                $applications       .= '</div>';
                $applications   .= '</div>';
                $applications .= '</div>';
			}
        }

        $this->set(compact('applications'));
	}
}