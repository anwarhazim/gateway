<?php

App::uses('AuthComponent', 'Controller/Component');

class Contact extends AppModel 
{
    public $actsAs = array('Tree');
    
	public $validate = array(
        'home_no' => array(
            'Numeric' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'The Home No can only contain numbers. Please try again!',
            ),
            'Minlength' => array(
                'rule' => array('minLength',10),
                'allowEmpty' => true,
                'message' => 'Minimum 10 digits only in Home No. Please try again!',
            ),
        ),
    );
    
    public $belongsTo = array(
		'Status' => array(
			'className' => 'Status',
			'fields' => array('name'),
			'foreignKey' => 'status_id',
		),
    );

    public function create_email()
	{
		return ($this->find('count', array('conditions' =>array('Contact.email' => $this->data[$this->alias]['email'], 'Contact.is_active' => 1))) == 0);
	}

	public function update_email()
	{
        return ($this->find('count', array('conditions' =>array('Contact.email' => $this->data[$this->alias]['email'], 'Contact.id !=' => $this->data[$this->alias]['id'], 'Contact.is_active' => 1))) == 0);
	}
    
    public function beforeSave($options = array()) 
	{
        if (!empty($this->data[$this->alias]['current_address_1']))
		{
			$this->data[$this->alias]['current_address_1'] = strtoupper($this->data[$this->alias]['current_address_1']);
        }

        if (!empty($this->data[$this->alias]['current_address_2']))
		{
			$this->data[$this->alias]['current_address_2'] = strtoupper($this->data[$this->alias]['current_address_2']);
        }

        if (!empty($this->data[$this->alias]['current_address_3']))
		{
			$this->data[$this->alias]['current_address_3'] = strtoupper($this->data[$this->alias]['current_address_3']);
        }
        
		return parent::beforeSave($options);
	}
}