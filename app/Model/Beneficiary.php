<?php

App::uses('AuthComponent', 'Controller/Component');

class Beneficiary extends AppModel 
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'percentage' => array(
			'notBlank' => array(
				'rule' => 'notBlank',
				'message' => 'The Percentage field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'Please insert number only in Percentage. Please try again!',
				),
			),
		'relationship' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Relationship field is required.'
				)
			),
		'contact_no' => array(
			'notBlank' => array(
				'rule' => 'notBlank',
				'message' => 'The Contact No. field is required.'
				),
			'Minlength' => array(
				'rule' => array('minLength', 8),
				'message' => 'Minimum 8 digits only in Contact No. Please try again!',
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'Please insert number only in Contact No. Please try again!',
				),
			),
		'house_address' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Address field is required.'
				)
			),
		'is_agreed' => array(
			'notEmpty' => array(
					'rule' => array('equalTo', '1'),
					'required' => true,
					'message' => 'You need to accept term and condition.'
				)
			),
	);

	public $belongsTo = array(
		'AgreedBy' => array(
			'className' => 'Employee',
			'fields' => array('complete_name'),
			'foreignKey' => 'agreed_by',
		),
		'CreatedBy' => array(
			'className' => 'Employee',
			'fields' => array('complete_name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Employee',
			'fields' => array('complete_name'),
			'foreignKey' => 'modified_by',
		)
    );
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['relationship']))
		{
			$this->data[$this->alias]['relationship'] = strtoupper($this->data[$this->alias]['relationship']);
		}

		if (!empty($this->data[$this->alias]['house_address']))
		{
			$this->data[$this->alias]['house_address'] = strtoupper($this->data[$this->alias]['house_address']);
		}
		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}