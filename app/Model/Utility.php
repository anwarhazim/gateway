<?php
App::uses('AuthComponent', 'Controller/Component');

class Utility extends AppModel 
{
	public $useTable = false;
	
	public function createFolder($filename=null)
	{
		$condition = false;

		if($filename)
		{
			if (!file_exists($filename)) 
			{
    			mkdir($filename, 0777);
    			$condition = true;
    		}
		}
		return $condition;
	}

	function encrypt( $id, $salt) 
	{
		$qEncoded = base64_encode($id . $salt);
		return( $qEncoded );
	}

	function decrypt( $key, $salt ) 
	{
		$qDecoded_raw = base64_decode($key);
		$qDecoded = preg_replace(sprintf('/%s/', $salt), '', $qDecoded_raw);

		return( $qDecoded );
	}

	public function generateRandomString($length = 10) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function getEmployeeSummary($employee_id = null)
	{
		$User = ClassRegistry::init('User');
		$Personal = ClassRegistry::init('Personal');
		$Contact = ClassRegistry::init('Contact');
		$Employee = ClassRegistry::init('Employee');
		$Gallery = ClassRegistry::init('Gallery');
		
		$data = array();

		if(!empty($employee_id))
		{
			$employee = $Employee->findById($employee_id);

			$user = $User->findById($employee['Personal']['user_id']);
			$personal = $Personal->findById($employee['Personal']['id']);

			$data['Employee']['id'] = "";
			$data['Employee']['complete_name'] = "";
			$data['Employee']['employee_no'] = "";
			$data['Employee']['email'] = "";
			$data['Employee']['organisation'] = "";
			$data['Employee']['avatar'] = "";
			

			if(!empty($employee))
			{
				$data['Employee']['id'] = $employee['Employee']['id'];
				$data['Employee']['employee_no'] = $employee['Employee']['employee_no'];

				$contact = $Contact->find('first', array(
														'conditions' => array(
																			'Contact.personal_id' => $employee['Personal']['id'],
																			'Contact.is_active' => 1,
														),
													));

				if(!empty($contact))
				{
					$data['Employee']['email'] = $contact['Contact']['email'];
				}

				if(!empty($employee['Organisation']))
				{
					$data['Employee']['organisation'] = $employee['Organisation']['name'];
				}
			}

			if(!empty($user))
			{
				$gallery = $Gallery->find('first', array(
														'conditions' => array(
																			'Gallery.user_id' => $user['User']['id'],
																			'Gallery.is_active' => 1,
														),
													));

				if(!empty($gallery['Gallery']['name']))
				{
					$data['Employee']['avatar'] = $gallery['Gallery']['name'];
				}
			}

			if(!empty($personal))
			{
				$data['Employee']['complete_name'] = $personal['Personal']['complete_name'];
			}
		}
		else
		{
			$data['Employee']['id'] = "0";
			$data['Employee']['complete_name'] = "System Administrator";
			$data['Employee']['employee_no'] = "00000000";
			$data['Employee']['email'] = "noreply@prasarana.com.my";
			$data['Employee']['organisation'] = "Prasarana Malaysia Berhad";
			$data['Employee']['avatar'] = "";
		}

		return $data;
	}

	public function getEmployeeInformation($employee_id = null)
	{
		$User = ClassRegistry::init('User');
		$Personal = ClassRegistry::init('Personal');
		$Contact = ClassRegistry::init('Contact');
		$Employee = ClassRegistry::init('Employee');
		$Gallery = ClassRegistry::init('Gallery');
		$Organisation = ClassRegistry::init('Organisation');
		
		$data = array();

		if(!empty($employee_id))
		{
			$employee = $Employee->findById($employee_id);
			$user = $User->findById($employee['Personal']['user_id']);
			$personal = $Personal->findById($employee['Personal']['id']);

			$data['User'] = array();
			$data['Gallery'] = array();
			$data['Personal'] = array();
			$data['Contact'] = array();
			$data['Gender'] = array();
			$data['Marital'] = array();
			$data['Ethnic'] = array();
			$data['Roles'] = array();
			$data['RoleList'] = array();
			$data['Employee'] = array();
			$data['EmployeeList'] = array();
			$data['Organisation'] = array();
			$data['PersonalArea'] = array();
			$data['PersonalSubarea'] = array();
			$data['EmployeeGroup'] = array();
			$data['EmployeeSubgroup'] = array();
			$data['PayrollArea'] = array();
			$data['CostCenter'] = array();
			$data['Position'] = array();
			$data['JobDesignation'] = array();
			$data['JobGrade'] = array();
			$data['Division'] = array();
			$data['Department'] = array();
			$data['Section'] = array();
			$data['Unit'] = array();
			$data['SubUnit'] = array();

			$data['Contact']['current_address_1'] = "";
			$data['Contact']['current_address_2'] = "";
			$data['Contact']['current_address_3'] = "";
			$data['Contact']['current_postcode'] = "";
			$data['Contact']['current_states_id'] = "";
			$data['Contact']['home_no'] = "";
			$data['Contact']['mobile_no'] = "";
			$data['Contact']['email'] = "";

			if(!empty($employee))
			{
				$data['Employee'] = $employee['Employee'];

				$contact = $Contact->find('first', array(
														'conditions' => array(
																			'Contact.personal_id' => $employee['Personal']['id'],
																			'Contact.is_active' => 1,
														),
													));

				if(!empty($contact))
				{
					$data['Contact'] = $contact['Contact'];
				}

				if(!empty($employee['Organisation']))
				{
					$data['Organisation'] = $employee['Organisation'];
				}

				if(!empty($employee['PersonalArea']))
				{
					$data['PersonalArea'] = $employee['PersonalArea'];
				}

				if(!empty($employee['PersonalSubarea']))
				{
					$data['PersonalSubarea'] = $employee['PersonalSubarea'];
				}

				if(!empty($employee['EmployeeGroup']))
				{
					$data['EmployeeGroup'] = $employee['EmployeeGroup'];
				}

				if(!empty($employee['EmployeeSubgroup']))
				{
					$data['EmployeeSubgroup'] = $employee['EmployeeSubgroup'];
				}

				if(!empty($employee['PayrollArea']))
				{
					$data['PayrollArea'] = $employee['PayrollArea'];
				}

				if(!empty($employee['CostCenter']))
				{
					$data['CostCenter'] = $employee['CostCenter'];
				}

				if(!empty($employee['Position']))
				{
					$data['Position'] = $employee['Position'];
				}

				if(!empty($employee['JobDesignation']))
				{
					$data['JobDesignation'] = $employee['JobDesignation'];
				}

				if(!empty($employee['JobGrade']))
				{
					$data['JobGrade'] = $employee['JobGrade'];
				}

				if(!empty($employee['Employee']['division_code']))
				{
					$organisation = $Organisation->find('first', array(
														'conditions' => array(
																			'Organisation.code' => $employee['Employee']['division_code'],
																			'BatchOrganisation.is_active' => 1,
														),
													));
					if(!empty($organisation))
					{
						$data['Division'] = $organisation['Organisation'];
					}
				}

				if(!empty($employee['Employee']['department_code']))
				{
					$organisation = $Organisation->find('first', array(
														'conditions' => array(
																			'Organisation.code' => $employee['Employee']['department_code'],
																			'BatchOrganisation.is_active' => 1,
														),
													));
					if(!empty($organisation))
					{
						$data['Department'] = $organisation['Organisation'];
					}

				}

				if(!empty($employee['Employee']['section_code']))
				{
					$organisation = $Organisation->find('first', array(
														'conditions' => array(
																			'Organisation.code' => $employee['Employee']['section_code'],
																			'BatchOrganisation.is_active' => 1,
														),
													));
					if(!empty($organisation))
					{
						$data['Section'] = $organisation['Organisation'];
					}
				}

				if(!empty($employee['Employee']['unit_code']))
				{
					$organisation = $Organisation->find('first', array(
														'conditions' => array(
																			'Organisation.code' => $employee['Employee']['unit_code'],
																			'BatchOrganisation.is_active' => 1,
														),
													));
					if(!empty($organisation))
					{
						$data['Unit'] = $organisation['Organisation'];
					}
				}

				if(!empty($employee['Employee']['subunit_code']))
				{
					$organisation = $Organisation->find('first', array(
														'conditions' => array(
																			'Organisation.code' => $employee['Employee']['subunit_code'],
																			'BatchOrganisation.is_active' => 1,
														),
													));
					if(!empty($organisation))
					{
						$data['SubUnit'] = $organisation['Organisation'];
					}
				}
			}

			if(!empty($user))
			{
				$data['User'] = $user['User'];

				for ($i=0; $i < count($user['Roles']) ; $i++) 
				{ 
					$data['Roles'][] = $user['Roles'][$i];
					$data['RoleList'][] = $user['Roles'][$i]['id'];
				}

				$gallery = $Gallery->find('first', array(
														'conditions' => array(
																			'Gallery.user_id' => $user['User']['id'],
																			'Gallery.is_active' => 1,
														),
													));

				if(!empty($gallery))
				{
					$data['Gallery'] = $gallery['Gallery'];
				}
			}

			if(!empty($personal))
			{
				$data['Personal'] = $personal['Personal'];
				$data['Gender'] = $personal['Gender'];
				$data['Ethnic'] = $personal['Ethnic'];

				foreach ($personal['Marital'] as $marital) 
				{
					$data['Marital']['id'] = $marital['id'];
					$data['Marital']['name'] = $marital['name'];
				}
			}

			$employees = $Employee->find('list', array(
													'conditions' => array(
																		'Employee.id' => $employee_id,
													),
												));

			foreach ($employees as $employee_id => $employee_name) 
			{
				$data['EmployeeList'][] = $employee_id;
			}
		}

		return $data;
	}

	public function getUserInformation($user_id = null)
	{
		$User = ClassRegistry::init('User');
		$Personal = ClassRegistry::init('Personal');
		$Contact = ClassRegistry::init('Contact');
		$Employee = ClassRegistry::init('Employee');
		$Gallery = ClassRegistry::init('Gallery');

		$data = array();

		if(!empty($user_id))
		{
			$user = $User->findById($user_id);
			$personal = $Personal->findByUserId($user_id);

			$data['User'] = array();
			$data['Gallery'] = array();
			$data['Personal'] = array();
			$data['Contact'] = array();
			$data['Gender'] = array();
			$data['Marital'] = array();
			$data['Ethnic'] = array();
			$data['Roles'] = array();
			$data['RoleList'] = array();
			$data['Employee'] = array();
			$data['EmployeeList'] = array();
			$data['Organisation'] = array();
			$data['PersonalArea'] = array();
			$data['PersonalSubarea'] = array();
			$data['EmployeeGroup'] = array();
			$data['EmployeeSubgroup'] = array();
			$data['PayrollArea'] = array();
			$data['CostCenter'] = array();
			$data['Position'] = array();
			$data['JobDesignation'] = array();
			$data['JobGrade'] = array();

			$data['Contact']['current_address_1'] = "";
			$data['Contact']['current_address_2'] = "";
			$data['Contact']['current_address_3'] = "";
			$data['Contact']['current_postcode'] = "";
			$data['Contact']['current_states_id'] = "";
			$data['Contact']['home_no'] = "";
			$data['Contact']['mobile_no'] = "";
			$data['Contact']['email'] = "";
			

			if(!empty($user))
			{
				$data['User'] = $user['User'];
			}

			for ($i=0; $i < count($user['Roles']) ; $i++) 
			{ 
				$data['Roles'][] = $user['Roles'][$i];
				$data['RoleList'][] = $user['Roles'][$i]['id'];
			}

			if(!empty($personal))
			{
				$data['Personal'] = $personal['Personal'];
				$data['Gender'] = $personal['Gender'];
				$data['Ethnic'] = $personal['Ethnic'];

				$contact = $Contact->find('first', array(
														'conditions' => array(
																			'Contact.personal_id' => $personal['Personal']['id'],
																			'Contact.is_active' => 1,
														),
													));

				if(!empty($contact))
				{
					$data['Contact'] = $contact['Contact'];
				}

				foreach ($personal['Marital'] as $marital) 
				{
					$data['Marital']['id'] = $marital['id'];
					$data['Marital']['name'] = $marital['name'];
				}

				$employee = $Employee->find('first', array(
															'conditions' => array(
																				'Employee.personal_id' => $personal['Personal']['id'],
																				'Employee.is_active' => 1,
															),
														));

				if(!empty($employee))
				{
					if(!empty($employee['Employee']))
					{
						$data['Employee'] = $employee['Employee'];
					}

					if(!empty($employee['Organisation']))
					{
						$data['Organisation'] = $employee['Organisation'];
					}

					if(!empty($employee['PersonalArea']))
					{
						$data['PersonalArea'] = $employee['PersonalArea'];
					}

					if(!empty($employee['PersonalSubarea']))
					{
						$data['PersonalSubarea'] = $employee['PersonalSubarea'];
					}

					if(!empty($employee['EmployeeGroup']))
					{
						$data['EmployeeGroup'] = $employee['EmployeeGroup'];
					}

					if(!empty($employee['EmployeeSubgroup']))
					{
						$data['EmployeeSubgroup'] = $employee['EmployeeSubgroup'];
					}

					if(!empty($employee['PayrollArea']))
					{
						$data['PayrollArea'] = $employee['PayrollArea'];
					}

					if(!empty($employee['CostCenter']))
					{
						$data['CostCenter'] = $employee['CostCenter'];
					}

					if(!empty($employee['Position']))
					{
						$data['Position'] = $employee['Position'];
					}

					if(!empty($employee['JobDesignation']))
					{
						$data['JobDesignation'] = $employee['JobDesignation'];
					}

					if(!empty($employee['JobGrade']))
					{
						$data['JobGrade'] = $employee['JobGrade'];
					}
				}

				$gallery = $Gallery->find('first', array(
														'conditions' => array(
																			'Gallery.user_id' => $user_id,
																			'Gallery.is_active' => 1,
														),
													));

				if(!empty($gallery))
				{
					$data['Gallery'] = $gallery['Gallery'];
				}

				$employees = $Employee->find('list', array(
														'conditions' => array(
																			'Employee.personal_id' => $personal['Personal']['id'],
														),
													));

				foreach ($employees as $employee_id => $employee_name) 
				{
					$data['EmployeeList'][] = $employee_id;
				}
			}
		}
		
		return $data;
	}

	public function getSidebar($id, $project_id)
	{
		$Setting = ClassRegistry::init('Setting');
		$Modul = ClassRegistry::init('Modul');
		$User = ClassRegistry::init('User');

		$sidebar = "";
		$modul_selected = array();
		$settings = array();
		$moduls = array();

		$data = $User->findById($id);

		foreach ($data['UserRole'] as $role) 
		{

			$settings = $Setting->find('all',
											array(
												'conditions' => array('role_id' => $role['role_id'])	
											));

			if($settings)
			{
				foreach ($settings as $setting) 
				{
					if (!in_array($setting['Setting']['modul_id'], $modul_selected)) 
					{
						array_push($modul_selected ,$setting['Setting']['modul_id']);
					}
				}
			}
		}
    	
    	$moduls = $Modul->find('threaded',
									array(
										'conditions' => array(
															'id' => $modul_selected,
															'Modul.is_active' => 1,
															'Modul.is_nav' => 1,
															'Modul.project_id' => $project_id
														),
										'order' => array('Modul.order asc')	
									));
									
		$sidebar = $this->getSidebarList($moduls, count($moduls), "");
		
		return $sidebar;
	}

	function getSidebarList($arr, $max, $sidebar)
    {
    	$base_url = Router::url('/', true);

    	if ( ! is_array($arr))
	        return $sidebar;

    	for ($i=0; $i < $max ; $i++) 
    	{ 
			if($arr[$i]['Modul']['is_dropdown'] == 1)
            {
                $sidebar .= "<li class='dropdown-submenu dropdown-submenu-hover'>";
            }
            else
            {
                $sidebar .= "<li>";
			}

			if($arr[$i]['Modul']['is_dropdown'] == 1)
            {
				if($arr[$i]['Modul']['is_type'] == 1)
        		{
					$sidebar .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
					$sidebar .= "<i class='".$arr[$i]['Modul']['icon']." dropdown-submenu dropdown-submenu-hover'></i>"; 
					$sidebar .= " ".$arr[$i]['Modul']['name']." <span class='caret'></span>";
					$sidebar .= "</a>";
				}
				else
				{
					$sidebar .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
					$sidebar .= "<i class='".$arr[$i]['Modul']['icon']." dropdown-submenu dropdown-submenu-hover'></i>"; 
					$sidebar .= " ".$arr[$i]['Modul']['name'];
					$sidebar .= "</a>";
				}
			}
			else
			{
				if($arr[$i]['Modul']['path'] == '#')
        		{
					$sidebar .= "<a href='#'>";
					$sidebar .= "<i class='".$arr[$i]['Modul']['icon']."'></i>";
					$sidebar .= " ".$arr[$i]['Modul']['name'];
					$sidebar .= "</a>";
				}
				else
				{
					$sidebar .= "<a href='".$base_url.$arr[$i]['Modul']['path']."'>";
					$sidebar .= "<i class='".$arr[$i]['Modul']['icon']."'></i>";
					$sidebar .= " ".$arr[$i]['Modul']['name'];
					$sidebar .= "</a>";
				}
			}

			if ( ! empty($arr[$i]['children']))
			{  
				$sidebar .= "<ul class='dropdown-menu width-200'>";
				$sidebar = $this->getSidebarList($arr[$i]['children'], count($arr[$i]['children']), $sidebar);
				$sidebar .= "</ul>";
			}
			
			$sidebar .= "</li>";
    	}

    	return $sidebar;
	}
	
	public function getProject($user_id = null)
	{	
		$Setting = ClassRegistry::init('Setting');
		$Modul = ClassRegistry::init('Modul');
		$User = ClassRegistry::init('User');

		$quicklink = "";
		$modul_selected = array();
		$project_selected = array();
		$settings = array();
		$moduls = array();

		$data = $User->findById($user_id);

		foreach ($data['UserRole'] as $role) 
		{

			$settings = $Setting->find('all',
											array(
												'conditions' => array('role_id' => $role['role_id'])	
											));

			if($settings)
			{
				foreach ($settings as $setting) 
				{
					if (!in_array($setting['Setting']['modul_id'], $modul_selected)) 
					{
						array_push($modul_selected ,$setting['Setting']['modul_id']);
					}
				}
			}
		}

		
    	
    	$moduls = $Modul->find('all',
									array(
										'conditions' => array(
															'Modul.id' => $modul_selected,
															'Modul.is_active' => 1,
															'Modul.is_nav' => 1,
														),
										'group' => 'Modul.project_id',
										'order' => array('Modul.order asc')	
									));

		foreach ($moduls as $modul) 
		{
			$project_selected[] = $modul['Modul']['project_id'];
		}

		$quicklink = $this->getQuickLink($project_selected, "");
									
		return $quicklink;
	}

	function getQuickLink($arr, $quicklink)
	{
		$Project = ClassRegistry::init('Project');

    	if ( ! is_array($arr))
			return $quicklink;
		
		foreach ($arr as $project_id) 
		{
			$project = $Project->findById($project_id);
			if(!empty($project))
			{
				$quicklink .= "<li class='media'>";
				$quicklink 		.= "<div class='media-left'>";
				$quicklink 			.= "<a href='".$project['Project']['url']."' class='btn border-".$project['Project']['color']." text-".$project['Project']['color']." btn-".$project['Project']['color']." btn-rounded btn-icon btn-sm' >";
				$quicklink 				.= "<i class='".$project['Project']['icon']."'></i>";
				$quicklink 			.= "</a>";
				$quicklink 		.= "</div>";
				$quicklink 		.= "<div class='media-body'>";
				$quicklink 			.= $project['Project']['name'];
				$quicklink 			.= "<div class='media-annotation'>".$project['Project']['desc']."</div>";
				$quicklink 		.= "</div>";
				$quicklink 	.= "</li>";
			}
		}

    	return $quicklink;
	}

	public function getDateFormatFromString($str = null)
	{

		$year = substr($str,0,4);
		$month = substr($str,4,2);
		$day = substr($str,6,4);

		$str = $day.'-'.$month.'-'.$year;
		
		return $str;
	}

	function datetime($datetime = null)
    {

        if(!empty($datetime))
        {
            $curr_date = strtotime(date("Y-m-d"));
            $the_date    = strtotime($datetime);
            $diff=floor(($curr_date-$the_date)/(60*60*24));

            switch($diff)
            {
                case 0:
                    $datetime = date("H:i a", strtotime($datetime));
                    break;
                default:
                    $datetime = date("d M", strtotime($datetime));
                    break;
            }
        }

        return $datetime;
	}
	
	function strlen($str = null, $len = 0)
    {

        if(strlen($str) > $len)
        {
            $str = substr($str,0,$len)."...";
        }

        return $str;
    }

	public function getULdatasource($datas = array(), $str = "", $model = "")
    {
        for ($i = 0; $i < count($datas) ; $i++) 
        {		
			$str = $str."<li>".$datas[$i][$model]['name'];
			
			if(!empty($datas[$i]['children']))
			{
				$str = $str."<ul>";
				$str = $this->getULdatasource($datas[$i]['children'], $str, $model);
				$str = $str."</ul>";
			}
			$str = $str."</li>";
		}
        return $str;
	}

	public function getThreaded($arr, $max , $model, $threaded = array())
	{
    	if ( ! is_array($arr))
	        return $threaded;

	    for ($i=0; $i < $max ; $i++) 
	    { 
	    	$threaded[] = $arr[$i][$model]['id'];

        	if ( ! empty($arr[$i]['children']))
	        {
	        	$threaded = $this->getThreaded($arr[$i]['children'] , count($arr[$i]['children']), $model ,$threaded);

	        }
	    }

	    return $threaded;
	}

	
	public function getNestable($arr, $max , $model, $nestable = "", $field)
	{
    	if ( ! is_array($arr))
	        return $nestable;

	    for ($i=0; $i < $max ; $i++) 
	    { 
	    	$nestable = $nestable.'<li class="dd-item" data-id="'.$arr[$i][$model]['id'].'">';

        	if(empty($arr[$i][$model]['code']))
        	{
        		$nestable = $nestable.'<div class="dd-handle"> #'.$arr[$i][$model]['id']." : ".$arr[$i][$model][$field].'</div>';
        	}
        	else 
        	{
        		$nestable = $nestable.'<div class="dd-handle"> #'.$arr[$i][$model]['id']." : ".$arr[$i][$model][$field].' ('.$arr[$i][$model]['code'].') ['.$arr[$i]['OrganisationCategory']['name'].']</div>';
        	}

        	if ( ! empty($arr[$i]['children']))
	        {
	        	$nestable = $nestable.'<ol class="dd-list">';
	        	$nestable = $this->getNestable($arr[$i]['children'] , count($arr[$i]['children']), $model ,$nestable, $field);
	        	$nestable = $nestable.'</ol>';
	        }
        	$nestable = $nestable.'</li>';
	    }

	    return $nestable;
	}
	
	public function getNestableSave($arr, $max, $model, $parent_id, $counter)
    {
    	$modul = ClassRegistry::init($model);
    	for ($i=0; $i < $max ; $i++) 
    	{ 
    		$data[$model]['id'] = $arr[$i]['id'];
            $data[$model]['parent_id'] = $parent_id;
            $data[$model]['order'] = $counter;
            $modul->create();
            $modul->save($data);
            $counter++;
            if(! empty($arr[$i]['children']))
            {
            	$this->getNestableSave($arr[$i]['children'] , count($arr[$i]['children']), $model, $arr[$i]['id'], $counter);
            }
    	}
	}

	public function getTreeModul($arr, $max , $tree = "")
    {
    	if ( ! is_array($arr))
	        return $tree;

	    $tree .= 	'<ul>';
	    for ($i=0; $i < $max ; $i++) { 
	    	$tree .=	'<li id="'.$arr[$i]['Modul']['id'].'" class="jstree-open">';
	    	$tree .=	$arr[$i]['Modul']['name'];
	    	if ( ! empty($arr[$i]['children']))
	        {
	        	$tree = $this->getTreeModul($arr[$i]['children'] , count($arr[$i]['children']), $tree);
	        }
	    	$tree .=	'</li>';
		}
		$tree .= 	'</ul>';	    

    	return $tree;
	}
}

?>