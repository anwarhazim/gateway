<?php

App::uses('AuthComponent', 'Controller/Component');

class Personal extends AppModel
{
	public $actsAs = array('Tree');

	public $validate = array(
        'complete_name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'ic_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The IC No. field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The IC No. field can only contain numbers. Please try again!',
				),
			'Maxlength' => array(
				'rule' => array('maxLength', 12),
				'message' => 'Maximum 12 digits only in IC No. Please try again!',
				),
			'Minlength' => array(
				'rule' => array('minLength', 12),
				'message' => 'Minimum 12 digits only in IC No. Please try again!',
				),
			'Unique'    => array(
				'rule'	=> array('create_ICNo'),
				'on' => 'create',
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
				),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_ICNo'),
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
				),
			),
		'is_type' => array(
			'notBlank' => array(
						'rule' => 'notBlank',
						'message' => 'Please enter is_type.'
				),
			),
		' is_flag' => array(
			'notBlank' => array(
						'rule' => 'notBlank',
						'message' => 'Please enter  is_flag.'
				),
			),
		'is_active' => array(
			'notBlank' => array(
						'rule' => 'notBlank',
						'message' => 'Please enter is_active.'
				),
			),
		'status_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Status.'
				),
			),
		'date_of_birth' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter Date of Birth.'
				),
			),
		'gender_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select Gender.'
				),
			),
		'ethnic_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select Ethnic.'
				),
			),
	);

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'fields' => array('id','username', 'status_id','is_active', 'modified', 'created'),
			'foreignKey' => 'user_id',
		),
		'Status' => array(
			'className' => 'Status',
			'fields' => array('id','name'),
			'foreignKey' => 'status_id',
		),
		'Gender' => array(
			'className' => 'Gender',
			'fields' => array('id','name'),
			'foreignKey' => 'gender_id',
		),
		'Ethnic' => array(
			'className' => 'Ethnic',
			'fields' => array('id','name'),
			'foreignKey' => 'ethnic_id',
		),
	);

	public $hasMany = array(
        'PersonalMarital' => array(
            'className' => 'PersonalMarital',
			'foreignKey' => 'personal_id',
			'conditions' => array('PersonalMarital.is_active' => 1)
        )
    );

    public $hasAndBelongsToMany = array(
        'Marital' => array(
            'className' => 'Marital',
            'joinTable' => 'personal_maritals',
        )
    );

	public function create_ICNo()
	{
		return ($this->find('count', array('conditions' =>array('Personal.ic_no' => $this->data[$this->alias]['ic_no'], 'Personal.is_active' => 1))) == 0);
	}

	public function update_ICNo()
	{
        return ($this->find('count', array('conditions' =>array('Personal.ic_no' => $this->data[$this->alias]['ic_no'], 'Personal.id !=' => $this->data[$this->alias]['id'], 'Personal.is_active' => 1))) == 0);
	}

	public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['complete_name']))
		{
			$this->data[$this->alias]['complete_name'] = strtoupper($this->data[$this->alias]['complete_name']);
		}

		if (!empty($this->data[$this->alias]['date_of_birth']))
		{
			$this->data[$this->alias]['date_of_birth'] = date("Y-m-d", strtotime($this->data[$this->alias]['date_of_birth']));
		}

		return parent::beforeSave($options);
	}

}
