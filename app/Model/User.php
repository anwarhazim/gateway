<?php

App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel 
{
    public $validate = array(
        'username' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Username field is required.'
                ),
            'Numeric' => array(
                'rule' => 'numeric',
                'message' => 'The Username field can only contain numbers. Please try again!',
                ),
            'Maxlength' => array(
                'rule' => array('maxLength', 8),
                'message' => 'Maximum 8 digits only in Username. Please try again!',
                ),
            'Minlength' => array(
                'rule' => array('minLength', 8),
                'message' => 'Minimum 8 digits only in Username Please try again!',
                ),
            'Unique'    => array(
                'rule'	=> array('create_Username'),
                'on' => 'create',
                'message' => 'Username already been used. Please try again!',
                'last' => false,
                ),
            'Update' => array(
                'on' => 'update',
                'rule' => array('update_Username'),
                'message' => 'Username already been used. Please try again!',
                'last' => false,
                ),
            ),
        'password' => array(
            'notBlank' => array(
                        'rule' => 'notBlank',
                        'message' => 'Please enter your new password.'
                ),
                'Length' => array(
                'rule'      => array('between', 6, 12),
                'message'   => 'Your password must contain at least 6 to 12 characters.',
            ),
        ),
        'current_password' => array(
            'notBlank' => array(
                        'rule' => 'notBlank',
                        'message' => 'Please enter your old password.'
                ),
            'Length' => array(
                'rule'      => array('between', 6, 12),
                'message'   => 'Your password must contain at least 6 to 12 characters.',
            ),
            'Check'    => array(
                'rule'      => array('checkCurrentPassword'),
                'message' => 'Your old password does not match.',
            )
        ),
        'c_password' => array(
            'notBlank' => array(
                        'rule' => 'notBlank',
                        'message' => 'Please re-enter your password.'
                ),
            'Length' => array(
                'rule'      => array('between', 6, 12),
                'message'   => 'Your re-enter password at least 6 to 12 characters.',
            ),
            'Compare'    => array(
                'rule'      => array('validate_passwords'),
                'message' => 'Your password does not match.',
            ),
        ),
    );
    
	public $hasMany = array(
        'UserRole' => array(
            'className' => 'UserRole',
            'foreignKey' => 'user_id',
        )
    );

    public $hasAndBelongsToMany = array(
        'Roles' => array(
            'className' => 'Role',
            'joinTable' => 'user_roles',
        )
    );

    public function create_Username()
	{
		return ($this->find('count', array('conditions' =>array('User.username' => $this->data[$this->alias]['username'], 'User.is_active' => 1))) == 0);
	}

	public function update_Username()
	{
        return ($this->find('count', array('conditions' =>array('User.username' => $this->data[$this->alias]['username'], 'User.id !=' => $this->data[$this->alias]['id'], 'User.is_active' => 1))) == 0);
    }
    
    public function checkCurrentPassword($data) 
    {
        $this->id = AuthComponent::user('id');
        $password = $this->field('password');
        return(AuthComponent::password($data['current_password']) == $password);
    }

    public function validate_passwords() 
    {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['c_password'];
    }

    public function beforeSave($options = array()) 
	{
		// hash our password
		if (isset($this->data[$this->alias]['password'])) 
		{
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		
		// if we get a new password, hash it
		if (isset($this->data[$this->alias]['password_update']) && !empty($this->data[$this->alias]['password_update'])) 
		{
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
		}
		// fallback to our parent
		return parent::beforeSave($options);
    }
}