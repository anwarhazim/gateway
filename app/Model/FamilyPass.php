<?php

App::uses('AuthComponent', 'Controller/Component');

class FamilyPass extends AppModel
{
	public $actsAs = array('Tree');

	public $validate = array(
		'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
            ),
		),
		'ic_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The IC No. field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The IC No. field can only contain numbers. Please try again!',
				),
			'Maxlength' => array(
				'rule' => array('maxLength', 12),
				'message' => 'Maximum 12 digits only in IC No. Please try again!',
				),
			'Minlength' => array(
				'rule' => array('minLength', 12),
				'message' => 'Minimum 12 digits only in IC No. Please try again!',
				),
			'Unique'    => array(
				'rule'	=> array('create_ICNo'),
				'on' => 'create',
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
				),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_ICNo'),
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
				),
			),
		'relationship_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Relationship.'
            ),
		),
		'family_pass_type_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Type.'
            ),
		),
		'family_pass_reason_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Reason.'
            ),
		),
	);

	public $belongsTo = array(
		'OrganisationType' => array(
			'className' => 'OrganisationType',
			'foreignKey' => 'organisation_type_id',
		),
		'Relationship' => array(
			'className' => 'Relationship',
			'fields' => array('id', 'name'),
			'foreignKey' => 'relationship_id',
		),
		'FamilyPassType' => array(
				'className' => 'FamilyPassType',
				'fields' => array('id', 'name'),
				'foreignKey' => 'family_pass_type_id',
			),
		'FamilyPassReason' => array(
				'className' => 'FamilyPassReason',
				'fields' => array('id', 'name'),
				'foreignKey' => 'family_pass_reason_id',
			),
		'Status' => array(
				'className' => 'Status',
				'fields' => array('id', 'name'),
				'foreignKey' => 'status_id',
			),
		'ApprovedBy' => array(
				'className' => 'Employee',
				'fields' => array('id', 'complete_name'),
				'foreignKey' => 'approved_by',
			),
		'CreatedBy' => array(
				'className' => 'Employee',
				'fields' => array('id', 'complete_name'),
				'foreignKey' => 'created_by',
			),
		'ModifiedBy' => array(
				'className' => 'Employee',
				'fields' => array('id', 'complete_name'),
				'foreignKey' => 'modified_by',
			)
	);

	public function create_ICNo()
	{
		return ($this->find('count', array('conditions' =>array('FamilyPass.ic_no' => $this->data[$this->alias]['ic_no'], 'FamilyPass.relationship_id' => $this->data[$this->alias]['relationship_id'], 'FamilyPass.is_active' => 1))) == 0);
	}

	public function update_ICNo()
	{
        return ($this->find('count', array('conditions' =>array('FamilyPass.id !=' => $this->data[$this->alias]['id'], 'FamilyPass.relationship_id' => $this->data[$this->alias]['relationship_id'], 'FamilyPass.is_active' => 1))) == 0);
	}

    public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['expiry']))
		{
			$this->data[$this->alias]['expiry'] = date("Y-m-d", strtotime($this->data[$this->alias]['expiry']));
		}
		
		if (!empty($this->data[$this->alias]['approved']))
		{
			$this->data[$this->alias]['approved'] = date("Y-m-d", strtotime($this->data[$this->alias]['approved']));
		}
		
		if (!empty($this->data[$this->alias]['expired']))
		{
			$this->data[$this->alias]['expired'] = date("Y-m-d", strtotime($this->data[$this->alias]['expired']));
		}
		
		if (!empty($this->data[$this->alias]['reject']))
		{
			$this->data[$this->alias]['reject'] = date("Y-m-d", strtotime($this->data[$this->alias]['reject']));
        }

		return parent::beforeSave($options);
	}
}
