<?php

App::uses('AuthComponent', 'Controller/Component');

class Notification extends AppModel 
{
	public $belongsTo = array(
		'Sender' => array(
			'className' => 'Employee',
			'foreignKey' => 'sender_id',
		),
		'Recipient' => array(
			'className' => 'Employee',
			'foreignKey' => 'recipient_id',
		),
		'CreatedBy' => array(
			'className' => 'Employee',
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Employee',
			'foreignKey' => 'modified_by',
		)
	);
	
    public function beforeSave($options = array()) 
	{	
		// fallback to our parent
		return parent::beforeSave($options);
	}
}