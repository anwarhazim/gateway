<?php

Router::connect('/', array('controller' => 'Notifications', 'action' => 'index'));
Router::connect('/login', array('controller' => 'Users', 'action' => 'login'));
Router::connect('/logout', array('controller' => 'Users', 'action' => 'logout'));

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';

