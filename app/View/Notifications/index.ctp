<div class="content-wrapper">

<!-- Cover area -->
<div class="profile-cover">
    <div class="profile-cover-img" style="background-image: url(<?php echo $this->html->url('/app/webroot/images/backgrounds/user_material_bg.jpg', true); ?>)">
    </div>
    <div class="media">
        <div class="media-left">
            <a href="#" class="profile-thumb">
                <?php
                    if(empty($session['Gallery']))
                    { 
                        echo $this->Html->image('/app/webroot/images/placeholders/placeholder.jpg', array('class' => 'img-circle', 'style' => 'width: 120px; height: 120px;', 'alt' => $session['Personal']['complete_name'])); 
                    }
                    else
                    {
                        echo $this->Html->image($session['Path']['url'].'/app/webroot/documents/'.$session['Employee']['employee_no'].'/MEDIAS/'.$session['Gallery']['name'], array('class' => 'img-circle', 'style' => 'width: 120px; height: 120px;', 'alt' => $session['Personal']['complete_name']));
                    }
                ?>
            </a>
        </div>

        <div class="media-body">
            <h1>
                <?php echo $session['Personal']['complete_name']; ?> 
                <small class="display-block"><?php echo $session['Organisation']['name']; ?></small>
                <small class="display-block"><?php echo $session['Employee']['position']; ?></small>
            </h1>
        </div>
    </div>
</div>
<!-- /cover area -->
<!-- Toolbar -->
<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
    
</div>
<!-- /toolbar -->

<div class="row">
    <div class="col-lg-12">
        <!-- Search -->
        <div class="panel panel-flat">
            <?php echo $this->Form->create('Notification', array('class'=>'', 'novalidate'=>'novalidate'));?>
            <div class="panel-body">
                <?php echo $this->Session->flash(); ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Search</label>
                            <?php 
                                echo $this->Form->input('search', array(
                                    'class'=>'form-control',
                                    'placeholder'=>'Search subject or content...', 
                                    'label'=> false,
                                    'error'=> false,
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Date</label>
                            <?php 
                                echo $this->Form->input('start_date', array(
                                    'class'=>'form-control start_date', 
                                    'placeholder'=>'dd-mm-yyyy.', 
                                    'label'=> false,
                                    'error'=> false,
                                    'data-mask' => '99-99-9999'
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>End Date</label>
                            <?php 
                                echo $this->Form->input('end_date', array(
                                    'class'=>'form-control end_date', 
                                    'placeholder'=>'dd-mm-yyyy', 
                                    'label'=> false,
                                    'error'=> false,
                                    'data-mask' => '99-99-9999'
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">
                        Find <i class="icon-search4 position-right"></i>
                    </button>
                    <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Notifications', true);?>">
                        Reset <i class="icon-spinner11 position-right"></i>
                    </a>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- /Search -->
        <!-- Result -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">My Notifications</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-inbox">
                    <tbody data-link="row" class="rowlink">
                        <?php
                            if(!empty($details))
                            {
                                foreach ($details as $notification) 
                                {
                                    $unread = "";
                                    if($notification['Notification']['is_read'] == 1)
                                    {
                                        $unread = "unread";
                                    }
                                    ?>
                                    <tr class="<?php echo $unread; ?>">
                                        <td class="table-inbox-image rowlink-skip">
                                            <a href="<?php echo $this->Html->url('/Notifications/view/'.$notification['Notification']['id'], true);?>">
                                                <?php
                                                    if(!empty($notification['Sender']['avatar']))
                                                    {
                                                        echo $this->Html->image($session['Path']['url'].'/app/webroot/documents/'.$notification['Sender']['employee_no'].'/MEDIAS/'.$notification['Sender']['avatar'], array('class' => 'img-circle img-xs', 'alt' => $notification['Sender']['complete_name']));
                                                    }
                                                    else
                                                    {
                                                        echo $this->Html->image('/app/webroot/images/placeholders/placeholder.jpg', array('class' => 'img-circle img-xs', 'alt' => $notification['Sender']['complete_name']));
                                                    }
                                                ?>
                                            </a>
                                        </td>
                                        <td class="table-inbox-name">
                                            <a href="#">
                                                <div  style="padding-left: 10px;" class="letter-icon-title text-default">
                                                    <?php echo $notification['Sender']['complete_name']; ?>
                                                </div>
                                            </a>
                                        </td>

                                        <td class="table-inbox-message">
                                            <div class="table-inbox-subject">
                                                <?php echo $notification['Notification']['subject']; ?>
                                            </div>
                                            <span class="table-inbox-preview">
                                                <?php echo $notification['Notification']['body']; ?>
                                            </span>
                                        </td>
                                        <td class="table-inbox-attachment">
                                            <!-- <i class="icon-attachment text-muted"></i> -->
                                        </td>
                                        <td class="table-inbox-time">
                                            <?php echo $notification['Notification']['created']; ?>
                                        </td>
                                    </tr>
                        <?php
                                }
                            }
                            else
                            {
                        ?>
                            <tr>
                                <td colspan="5">
                                    No Notification...
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    <ul class="pagination  pull-right">
                        <?php 
                            echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
                            echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Result -->
    </div>
</div>

</div>