<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Gateway | Prasarana Self-Service</title>

	<!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/icons/icomoon/styles.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/bootstrap.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/core.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/components.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/colors.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/jstree/jstree.min.css', true); ?>">
	<!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/loaders/pace.min.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery.min.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/bootstrap.min.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/loaders/blockui.min.js', true); ?>"></script>
	<!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/styling/uniform.min.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/app.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/demo_pages/login.js', true); ?>"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

                <?php echo $this->fetch('content'); ?>

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	<div class="footer text-muted text-center">
		&copy; 2020 <a href="#">Gateway | Prasarana Self-Service</a> by <a href="http://www.myrapid.com.my/corporate-information" target="_blank">Prasarana</a> powered by D&T Division
	</div>
	<!-- /footer -->

</body>
</html>