<div class="content-wrapper">

<!-- Search -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Employee', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php echo $this->Session->flash(); ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Search</label>
                    <?php 
                        echo $this->Form->input('search', array(
                            'class'=>'form-control',
                            'placeholder'=>'Name/ Staff No.', 
                            'label'=> false,
                            'error'=> false,
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Find <i class="icon-search4 position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Employees/colleague', true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Search -->
<!-- Result -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">Latest connections</h6>
    </div>
    <div class="panel-body">
        <?php
            $counter = 1;
            foreach ($details as $detail) 
            {
                if($counter == 1)
                {
                    ?>
                        <div class="row">
                    <?php
                }
        ?>
        <div class="col-lg-3 col-md-4">
            <div class="thumbnail">
                <div class="thumb thumb-rounded">
                    <?php echo $this->Html->image($detail['Gallery']['name'], array('style' => 'width: 90px; height: 90px;')); ?>
                </div>
            
                <div class="caption text-center">
                    <h6 class="text-semibold no-margin">
                        <?php echo $detail['Personal']['complete_name']; ?> 
                        <small class="display-block"><?php echo $detail['Employee']['employee_no']; ?></small>
                        <small class="display-block"><?php echo $detail['Organisation']['name']; ?></small>
                        <small class="display-block"><?php echo $detail['Employee']['position']; ?></small>
                    </h6>
                </div>
            </div>
        </div>
        <?php
                if($counter == 4)
                {
                    ?>
                        </div>
                    <?php
                }

                $counter++;

                if($counter == 5)
                {
                    $counter = 1;
                }
            }
        ?>
        <span class="pull-right">
            <?php echo $this->Paginator->counter(array(
                                                'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                        {:count} total, start {:start}, end at {:end}'
                                                ));
            ?>
        </span>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <ul class="pagination  pull-right">
                <?php 
                    echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
                    echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</div>
<!-- /Result -->

</div>