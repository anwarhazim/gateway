<div class="content-wrapper">

<div class="row">
    <div class="col-lg-12">
        <!-- Search -->
        <div class="panel panel-flat">
            <?php echo $this->Form->create('Employee', array('class'=>'', 'novalidate'=>'novalidate'));?>
            <div class="panel-body">
                <?php echo $this->Session->flash(); ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <legend class="text-bold">Personal Information</legend>
                        </div>
                        <div class="col-md-6">
                            <label>Name</label>
                            <?php 
                                echo $this->Form->input('complete_name', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Personal']['complete_name'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>IC No.</label>
                            <?php 
                                echo $this->Form->input('ic_no', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Personal']['ic_no'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Gender</label>
                            <?php 
                                echo $this->Form->input('gender_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'options'=>$genders,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Personal']['gender_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>Ethnic</label>
                            <?php 
                                echo $this->Form->input('ethnic_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'options'=>$ethnics,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Personal']['ethnic_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Date of Birth</label>
                            <?php 
                                echo $this->Form->input('date_of_birth', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Personal']['date_of_birth'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>Marital Status</label>
                            <?php 
                                echo $this->Form->input('marital_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'options'=>$marital,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Marital']['id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <legend class="text-bold">Contact Information</legend>
                        </div>
                        <div class="col-md-12">
                            <label>Address</label>
                            <?php 
                                echo $this->Form->input('current_address_1', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['current_address_1'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <?php 
                                echo $this->Form->input('current_address_2', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['current_address_2'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <?php 
                                echo $this->Form->input('current_address_3', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['current_address_3'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Postcode</label>
                            <?php 
                                echo $this->Form->input('current_postcode', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['current_postcode'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>State</label>
                            <?php 
                                echo $this->Form->input('current_states_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'options'=>$states,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Contact']['current_states_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>No. Tel. (Home)</label>
                            <?php 
                                echo $this->Form->input('home_no', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['home_no'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-4">
                            <label>No. Tel. (HP)</label>
                            <?php 
                                echo $this->Form->input('mobile_no', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['mobile_no'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-4">
                            <label>Email</label>
                            <?php 
                                echo $this->Form->input('email', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['email'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <legend class="text-bold">Employment Information</legend>
                        </div>
                        <div class="col-md-3">
                            <label>Staff No.</label>
                            <?php 
                                echo $this->Form->input('employee_no', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Employee']['employee_no'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label>Designation</label>
                            <?php 
                                echo $this->Form->input('job_designation_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$jobdesignations,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['job_designation_id'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label>Job Grade</label>
                            <?php 
                                echo $this->Form->input('job_grade_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$jobgrades,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['job_grade_id'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-3">
                            <label>Position</label>
                            <?php 
                                echo $this->Form->input('position', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Organisation Unit</label>
                            <?php 
                                echo $this->Form->input('organizational_unit_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$organisations,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['organizational_unit_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Personal Area</label>
                            <?php 
                                echo $this->Form->input('personal_area_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$personalareas,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['personal_area_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>Personal Subarea</label>
                            <?php 
                                echo $this->Form->input('personal_subarea_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$personalsubareas,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['personal_subarea_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Employee Group</label>
                            <?php 
                                echo $this->Form->input('employee_group_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$employeegroups,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['employee_group_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>Employee Subgroup</label>
                            <?php 
                                echo $this->Form->input('employee_subgroup_id', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'options'=>$employeesubgroups,
                                    'empty'=>'PLEASE SELECT...',
                                    'value'=>$employee['Employee']['employee_subgroup_id'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Seniority Date</label>
                            <?php 
                                echo $this->Form->input('seniority_date', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Employee']['seniority_date'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                        <div class="col-md-4">
                            <label>Entry Date</label>
                            <?php 
                                echo $this->Form->input('entry_date', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Employee']['entry_date'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                        <div class="col-md-4">
                            <label>Leaving Date</label>
                            <?php 
                                echo $this->Form->input('leaving_date', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Employee']['leaving_date'],
                                    'disabled'=>$disabled
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- /Search -->
    </div>
</div>

</div>