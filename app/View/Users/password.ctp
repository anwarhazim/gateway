<div class="content-wrapper">

<!-- Cover area -->
<div class="profile-cover">
    <div class="profile-cover-img" style="background-image: url(<?php echo $this->html->url('/app/webroot/images/backgrounds/user_material_bg.jpg', true); ?>)">
    </div>
    <div class="media">
        <div class="media-left">
            <a href="#" class="profile-thumb">
                <?php
                    if(empty($session['Gallery']))
                    { 
                        echo $this->Html->image('/app/webroot/images/placeholders/placeholder.jpg', array('class' => 'img-circle', 'style' => 'width: 120px; height: 120px;', 'alt' => $session['Personal']['complete_name'])); 
                    }
                    else
                    {
                        echo $this->Html->image($session['Path']['url'].'/app/webroot/documents/'.$session['Employee']['employee_no'].'/MEDIAS/'.$session['Gallery']['name'], array('class' => 'img-circle', 'style' => 'width: 120px; height: 120px;', 'alt' => $session['Personal']['complete_name']));
                    }
                ?>
            </a>
        </div>

        <div class="media-body">
            <h1>
                <?php echo $session['Personal']['complete_name']; ?> 
                <small class="display-block"><?php echo $session['Organisation']['name']; ?></small>
                <small class="display-block"><?php echo $session['Employee']['position']; ?></small>
            </h1>
        </div>
    </div>
</div>
<!-- /cover area -->
<!-- Toolbar -->
<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
    <ul class="nav navbar-nav visible-xs-block">
        <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
    </ul>
    <!--
    <div class="navbar-collapse collapse" id="navbar-filter">
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-books"></i> User Manual <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header highlight"><i class="icon-file-eye"></i> Guidelines</li>
                    <li><a href="#" target="_blank">PRESS User Manual</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    -->
</div>
<!-- /toolbar -->

<div class="row">
    <div class="col-lg-12">
        <!-- Search -->
        <div class="panel panel-flat">
            <?php echo $this->Form->create('User', array('class'=>'', 'novalidate'=>'novalidate'));?>
            <div class="panel-heading">
                <h6 class="panel-title">Change Password</h6>
            </div>
            <div class="panel-body">
                <?php 
                    echo $this->Session->flash();
                    
                    if(!empty($this->validationErrors['User']))
                    {
                    ?>
                        <div role="alert" class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <?php
                                    foreach ($this->validationErrors['User'] as $errors) 
                                    {
                                        echo '<ul>';
                                        foreach ($errors as $error) 
                                        {
                                            echo '<li>'.h($error).'</li>';
                                        }
                                        echo '</ul>';
                                    }
                                ?>
                        </div>
                    <?php
                    }
                ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Old Password <span class="text-danger">*</span></label>
                            <?php 
                                echo $this->Form->input('current_password', array(
                                    'class'=>'form-control',
                                    'type'=>'password',
                                    'placeholder'=>'Your current password', 
                                    'label'=> false,
                                    'error'=> false,
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>New Password <span class="text-danger">*</span></label>
                            <?php 
                                echo $this->Form->input('password', array(
                                    'class'=>'form-control',
                                    'type'=>'password',
                                    'placeholder'=>'Your new password', 
                                    'label'=> false,
                                    'error'=> false,
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Re-enter Password <span class="text-danger">*</span></label>
                            <?php 
                                echo $this->Form->input('c_password', array(
                                    'class'=>'form-control',
                                    'type'=>'password',
                                    'placeholder'=>'Your re-enter password', 
                                    'label'=> false,
                                    'error'=> false,
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-success">
                        Update <i class="icon-floppy-disk position-right"></i>
                    </button>
                    <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Users/password', true);?>">
                        Reset <i class="icon-spinner11 position-right"></i>
                    </a>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- /Search -->
    </div>
</div>

</div>