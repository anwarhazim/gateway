<!-- Simple login form -->
<?php echo $this->Form->create('User', array('novalidate'=>'novalidate')); ?>
    <div class="panel panel-body login-form">
        <div class="text-center">
			<a href="<?php echo $this->Html->url('/', true);?>">
                <?php echo $this->Html->image('/app/webroot/images/logos/logo_gateway_small.png', array('alt' => 'Gateway | Prasarana Self-Service', 'width'=>'', 'height'=>'38px')); ?>
			</a>
		</div>
        <div class="text-center">
            <div class="icon-object border-primary-800 text-primary-800"><i class="icon-enter2"></i></div>
            <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
        </div>

        <?php echo $this->Session->flash(); ?>

        <div class="form-group has-feedback has-feedback-left">
			<?php echo $this->Form->input('username', array('class'=>'form-control', 'placeholder'=>'Staff No.', 'label'=> false )); ?>
			<div class="form-control-feedback">
				<i class="icon-user text-muted"></i>
			</div>
        </div>
        
        <div class="form-group has-feedback has-feedback-left">
            <?php echo $this->Form->password('password', array('class'=>'form-control', 'placeholder'=>'Password', 'label'=> false )); ?>
            <div class="form-control-feedback">
                <i class="icon-lock2 text-muted"></i>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
<!-- /simple login form -->