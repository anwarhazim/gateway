<div role="alert" class="alert alert-danger">
    <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <?php echo h($message); ?>
</div>